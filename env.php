<?php
  $variables = [
      'APP_KEY' => '',
      'DB_HOST' => 'localhost',
      'DB_USERNAME' => 'root',
      'DB_PASSWORD' => '',
      'DB_NAME' => 'as_db',
      'DB_PORT' => '8080',
  ];
  foreach ($variables as $key => $value) {
      putenv("$key=$value");
  }
?>
