<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../config/database.php';
include_once '../objects/product.php';
 
$database = new Database();
$db = $database->getConnection();
 

$product = new Product($db);
 

$stmt = $product->read();
$num = $stmt->rowCount();
 

if($num>0){
 
    
    $products_arr=array();
    $products_arr["records"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $product_item=array(
            "record_id" => $record_id,
            "student_id" => $student_id,
            "student_name" => $student_name,
            "exam_name" => $exam_name,
            "exam_id" => $exam_id,
            "score" => $score,
            "status" => $status,
            "next_retake" => $next_retake,
            "date" => $date
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
    http_response_code(200);
 
    echo json_encode($products_arr);
    file_put_contents('assessment.json', serialize($products_arr));
    $products_arr = unserialize(file_get_contents('assessment.json'));
}
 
else{
 

    http_response_code(404);
 
    echo json_encode(
        array("message" => "No products found.")
    );
}