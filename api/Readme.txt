**Untuk menggunakan API, gunakan aplikasi Postman**
(https://www.getpostman.com/downloads/)
1. Letakkan seluruh folder api dan folder "Online Examination"di folder htdocs pada xampp
2. Buka aplikasi postman, pastikan layanan apache dan MySQL sudah dinyalakan, dan database pada folder "Online Examination" sudah di extract ke PHPMyAdmin
3. Dalam aplikasi postman, lakukan perintah GET dengan perintah :
(http://localhost/api/product/read.php)
4. di dalam folder "product" dalam folder api akan ada file dengan nama "assessment.json"
5. file "assessment.json" berisi data dari database "tbl_assessment_records".