<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "tbl_assessment_records";
 
    // object properties
    public $record_id;
    public $student_id;
    public $student_name;
    public $exam_name;
    public $exam_id;
    public $score;
    public $status;
    public $next_retake;
    public $date;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
// read products
function read(){
 
    // select all query
    $query = "SELECT *
            FROM
                " . $this->table_name . " p";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
    echo json_encode($stmt);
}
}